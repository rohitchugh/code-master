#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H
#include <iostream>

using namespace std;

template <typename V>
class SinglyLinkedList;

template <typename type>
class Node{
	type data;
	Node * next;
	public:
		Node() : next(0), data(0) {}
		Node(const type &d) : next(0), data(d) {}
		Node(const Node &n) : next(d.next), data(n.data) {}

		type getData() const{
			return data;
		}

		template <typename U>
			friend class SinglyLinkedList;
		template <typename U>
			friend ostream & operator<<(ostream &, const SinglyLinkedList<U> &);
}

template <typename T>
class SinglyLinkedList{
	Node<T> *head;
	static int sharedMemory;

	//COPY HELPER
	void copyAs(const SinglyLinkedList &L){
		//CANNOT CHANGE THE DATA POINTED TO BY THIS POINTER(CONST)
		//BUT CAN CHANGE THE POINTER VALUE
		const Node<T> *it = L.head;
		Node<T> *tail = 0;
		while(it){
			Node<T> *temp = new Node<T>(*it);
			if(!head){
				head = tail = temp;
			} else {
				tail->next = temp;
				tail = temp;
			}
			it = it->next;
		}
		return;
	}

	public:
		SinglyLinkedList(): head(0) {}
		SinglyLinkedList(const SinglyLinkedList &L) : head(0){
			copyAs(L);
		}

		// Clear LL
		void clear(){
			while(head){
				Node<T> *temp = head;
				head = head->next;
				delete temp;
			}
		}
	
		~SinglyLinkedList(){
			clear();
			//(*this).clear();
			//this->clear;
		}

		// ++ OPEARATOR OVERLOADING
		// PRE-INCREMENT OPERATOR
		SinglyLinkedList & operator++(){
			Node<T> * it = head;
			while(it){
				it->data++;
				it = it->next;
			}
			return *this;
		}

		// ++(INT DUMMY) OPERATOR OVERLOADING
		// POST-INCREMENT OPERATOR
		SinglyLinkedList & operator++(int dummy){
			SinglyLinkedList temp = *this;
			++(*this);
			return temp;
		}

		// = OPERATOR OVERLOADING
		SinglyLinkedList & operator=(const SinglyLinkedList &L){
			clear();
			copyAs(L);
			return *this;
		}

		// += OPERATOR OVERLOADING
		SinglyLinkedList & operator+=(const T &el){
			insertAtTheEnd(el);
			return *this;
		}

		// +=(CONST SINGLYLINKEDLIST &L) OPERATOR LOADING
		SinglyLinkedList &operator+=(const SinglyLinkedList &L){
			SinglyLinkedList temp(L);
			if(!head){
				head = temp.head;
			} else {
				Node<T> *it = head;
				while(it->next){
					it = it->next;
				}
				it->next = temp.head;
			}
			temp.head = 0;
			return *this;
		}

		// +(CONST SINGLYLINKEDLIST &L) OPERATOR OVERLOADING
		SinglyLinkedList operator+(const SinglyLinkedList &L){
			SinglyLinkedList ans = *this;
			SinglyLinkedList ansright = L;
			if(!ans.head)
				return ansright;

			Node<T> *temp = ans.head;
			while(temp->next)
				temp = temp->next;

			temp->next = ansright.head;
			ansright.head = 0;
			return ans;
		}

		//FIND ELEMENT AT POS K
		Node<T> &findElementAtPosK(T &k){
			Node *temp = head;
			k--;
			while(k--){
				temp = temp->next;
			}
			return *temp;
		}
		
		//FIND Kth NODE FROM THE END | K < LENGTH OF LL
		Node<T> &findKthNodeFromEnd(T &k){
			Node<T> *it1 = head;
			Node<T> *it2 = head;
			while(k--)
				it2 = it2->next;

			while(it2){
				it1 = it1->next;
				it2 = it2->next;
			}

			return *it1;
		}

		//THIS SOLUTION IS ALSO TRUE
		/*
		Node<T> &findKthNodeFromEnd(T &k){
			int len = 1;
			Node<T> *temp = head;
			while(temp){
				len++;
				temp = temp->next;
			}
			return findElementAtPosK(len-k+1);
		}
		*/

		//INSERT AT THE HEAD
		void insertAtTheHead(const T &d){
			Node<T> *temp = new Node<T>(d);
			temp->next = head;
			head = temp;
			return;
		}

		//DELETE FROM HEAD
		void deleteFromHead(){
			if(!head) return;
			Node<T> *temp = head;
			head = head->next;
			delete temp;
			return;
		}

		//INSERT AT THE END
		void insertAtTheEnd(const T&); //Initialization at last

		//DELETE FROM TAIL/END
		void deleteFromEnd(){
			if(!head) return;
			Node<T> *temp = head;
			if(temp->next == NULL){
				head = 0;
				delete temp;
				return;
			}

			while(temp->next->next){
				temp = temp->next;
			}
			delete temp->next;
			temp->next = NULL;
			return;
		}

		//INSERT AT POS K | STARTING INDEX IS 0
		void insertAtPosK(const T &d, T &k){
			if(k < 0){
				cout << "Negative Positions are not permitted" << endl;
				return;
			}

			Node<T> *temp = new Node<T>(d);
			if(k == 0){
				temp->next = head;
				head = temp;
				return;
			}
			Node<T> *it = head;
			k--;
			while(k--){
				it = it->next;
				if(it == NULL){
					cout << "Position is beyond LL length" << endl;
					return;
				}
			}
			temp->next = it->next;
			it->next = temp;
			return;
		}

		//DELETE AT POS K | STARTING INDEX IS 0
		void deleteAtPosK(T &k){
			if(k==0){
				Node<T> *temp = head;
				head = head->next;
				delete temp;
				return;
			}

			Node<T> *it = head;
			k--;
			while(k--){
				it = it->next;
				if(it == NULL){
					cout << "Position does not exist in LL" << endl;
					return;
				}
			}
			Node<T> *temp = it->next;
			it->next = temp->next;
			delete temp;
			return;
		}

		//DELETE AT POS K FROM END
		void deleteAtPosKFromEnd(T &k){
			if(!head) return;

			Node<T> *it1 = head;
			Node<T> *it2 = head;

			while(k--){
				it2 = it2->next;
				if(it2 == NULL && k==0){
					Node<T> *temp = head;
					head = head->next;
					delete temp;
					return;
				}
				if(it2 == NULL)
					return;
			}

			while(it2->next != NULL){
				it1 = it1->next;
				it2 = it2->next;
			}

			Node<T> *temp = it1->next;
			it1->next = temp->next;
			delete temp;
			return;
		}

		//FIND MIDDLE
		Node<T> *findMiddle() const {
			if(!head)
				return NULL;

			Node<T> *it1 = head;
			Node<T> *it2 = head;

			while(it2 != NULL && it2->next != NULL){
				it1 = it1->next;
				it2 = it2->next->next;
			}
			return it1;
		}

		//PRINT REVERSE LL
		void printReverse() const {
			printReverseHelper(head);
			cout << "NULL" << endl;
		}

		private:
		static void printReverseHelper(const Node<T> *head){
			if(!head) return;
			printReverseHelper(head->next);
			cout << head->data << "-->"
			return;
		}

		public:
		//PRINT LL
		void print() const {
			Node<T> *it =head;
			while(it){
				cout << it->data << " --> ";
				it = it->next;
			}
			cout << "NULL" << endl;
			return;
		}

		//MERGE TWO SORTED LL
		SinglyLinkedList merge(SinglyLinkedList &L){
			if(!head)
				return L.head;

			if(!L.head)
				return head;

			Node<T> *it1 = head;
			Node<T> *it2 = L.head;
			SinglyLinkedList Ltemp;

			while(it1 && it2){
				if(it1->data < it2->data){
					Ltemp.insertAtTheEnd(it1->data);
					it1 = it1->next;
				} else {
					Ltemp.insertAtTheEnd(it2->data);
					it2 = it2->next;
				}
			}

			while(it1){
				Ltemp.insertAtTheEnd(it1->data);
				it1 = it1->next;
			}

			while(it2){
				Ltemp.insertAtTheEnd(it2->data);
				it2 = it2->next;
			}

			return Ltemp;
		}

		//MERGE K SORTED LISTS
		Node<T> *mergeKLists(vector<SinglyLinkedList*> &lists){
			if(lists.size() < 1)
				return NULL;

			while(lists.size() > 1){
				lists.push_back(mergeTwoLists(lists[0], lists[1]));
				lists.erase(lists.front());
				lists.erase(lists.front());
			}
			return lists.front();
		}

		//MERGE TWO LISTS
		Node<T> *mergeTwoLists(Node<T> *l1, Node<T> *l2){
			if(!l1)
				return l2;

			if(!l2)
				return l1;

			Node<T> *ans = NULL;
			Node<T> *prev = NULL
			while(l1 && l2){
				if(l1->data < l2->data){
					if(!ans)
						ans = prev = new Node<T>(l1->data);
					else{
						prev->next = new Node<T>(l1->data);
						prev = prev->next;
					}
				}
				else{
					if(!ans)
						ans = prev = new Node<T>(l2->data);
					else{
						prev->next = new Node<T>(l2->data);
						prev = prev->next;
					}
					l2 = l2->next;
				}
			}

			while(l1){
				if(!ans)
					ans = prev = new Node<T>(l1->data);
				else{
					prev->next = new Node<T>(l1->data);
					prev = prev->next;
					l1 = l1->next;
				}
			}

			while(l2){
				if(!ans)
					ans = prev = new Node<T>(l2->data);
				else{
					prev->next = new Node<T>(l2->data);
					prev = prev->next;
					l2 = l2->next;
				}
			}
		}

		//GET LENGTH OF LL
		int length(){
			if(!head) return 0;
			Node<T> *temp = head;
			int count = 0;
			while(temp){
				count++;
				temp = temp->next;
			}
			return count;
		}

		//REVERSE LL
		void reverse(){
			Node<T> *prev = NULL;
			Node<T> *current = head;
			Node<T> *next = NULL;

			while(current){
				next = current->next;
				current->next = prev;
				prev = current;
				current = next;
			}

			head = prev;
		}

		//REVERSE LL RECURSIVELY
		void reverseRecursive(){
			reverseRecursiveHelper(head);
		}

		private:
		void reverseRecursiveHelper(Node<T> *head){
			if(!head)
				return;

			/*
				Suppose first ={1,2,3} , rest={2,3}
			*/
			Node<T> *first = head;
			Node<T> *rest = first->next;
			
			// List has only one node | Set Last node to head
			if(!rest){
				head = first;
				return;
			}

			//Reverse the rest list and & put the first
			// element at the end
			reverseRecursiveHelper(rest);

			first->next->next = first;

			//Tricky Step
			// 1->2<-3<-4 ==> NULL<-1<-2<-3<-4
			first->next = NULL;
		}

		public:
		void reverseInGroupsOfK(const T &k){
			head = reverseInGroupsOfKHelper(head, k);
		}

		private:
		Node<T> *reverseInGroupsOfKHelper(Node<T> *head, const T k){
			Node<T> *prev = NULL;
			Node<T> *current = head;
			Node<T> *next = NULL;

			T group = k;

			while(current != NULL && group--){
				next = current->next;
				current->next = prev;
				prev = current;
				current = next;
			}

			if(next != NULL){
				head->next = reverseInGroupsOfKHelper(current->next, k);
			}

			return prev;
		}

		public:
		//REVERSE FROM HEAD TO Kth NODE
		void reverseTillK(const T &K) const{
			reverseTillKHelper(head, k);
		}

		private:
		void reverseTillKHelper(Node<T> *head, T k){
			Node<T> *prev = NULL;
			Node<T> *current = head;
			Node<T> *next = NULL;

			while(current != NULL && k--){
				next = current->next;
				current->next = prev;
				prev = current;
				current = next;
			}

			head->next = current;
			head = prev;
		}

		public:
		//REVERSE FROM M TO N
		void reverseFromMToN(const T &m, const T &n){
			if(m==1)
				reverseTillKHelper(head, n);
			else{
				Node<T> *temp = head;
				T it =m-2;
				while(it--)
					temp = temp->next;
				reverseFromMToNHelper(temp, n-m+1);
			}
		}

		private:
		void reverseFromMToNHelper(Node<T> *node, T m){
			Node<T> *temp = node;
			Node<T> *prev = NULL;
			Node<T> *current = node->next;
			Node<T< *next = NULL;

			while(current != NULL && m--){
				next = current->next;
				current->next = prev;
				prev = current;
				current = next;
			}

			temp->next->next = current;
			temp->next = prev;
		}

		public:
		//DETECT LOOP IN LL
		bool detectLoop() {
			if(!head) return false;

			Node<T> *slow = head;
			Node<T> *fast = head;

			if(fast && fast->next){
				slow = slow->next;
				fast = fast->next->next;
				if(slow == fast)
					return true;
			}

			return false;
		}

		//FIND LOOP NODE
		Node<T> *findLoopNode(){
			Node<T> *slow = head;
			Node<T> *fast = head;
			bool loopExists = false;

			while(fast && fast->next){
				slow = slow->next;
				fast = fast->next->next;
				if(slow == fast){
					loopExists = true;
					break;
				}
			}

			if(loopExists){
				slow = head;
				while(slow != fast){
					slow = slow->next;
					fast = fast->next;
				}
				return slow;
			}
			return NULL;
		}

		//REMOVE LOOP
		void removeLoop(){
			Node<T> *loopNode = this->findLoopNode();

			if(loopNode == NULL)
				return;

			Node<T> *temp = loopNode->next;
			while(temp->next != loopNode)
				temp = temp->next;

			temp->next = NULL;
		}

		//COPY LIST WITH RANDOM POINTER
		/*
			Definition for singly-linked list with random ptr
			class RandomListNode{
				int label;
				RandomListNode *next, *random;
				RandomListNode(int x) : label(x), next(0), random(0){}
			}

			RandomListNode *copyRandomList(RandomListNode *head){
				if(!head)
					return head;

				map<RandomListNode*, RandomListNode*> myMap;

				RandomListNode *temp = head;
				while(temp){
					myMap.insert({
						temp,
						new RandomListNode(temp->label)
					});
					temp = temp->next;
				}

				temp = head;
				while(temp){
					myMap[temp]->next = myMap[temp->next];
					myMap[temp]->random = myMap[temp->random];
					temp = temp->next;
				}

				return myMap[head];
			}
		*/

		//ROTATE LINKED LIST
		/*
			This function rotates a linked list counter-clockwise and
			updates the head. The function assumes that k is smaller
			than size of linked list. It doesn't modify the list if k
			is greater than or equal to size.
		*/
		void rotate(Node<T> *head, int k){
			if(k==0)
				return;

			//Let k =3 and :: = 10->20->30->40->50->60
			Node<T> *current = head;

			// Get Kth Node
			int count = 1;
			while(count < k && current != NULL){
				current = current->next;
				count++;
			} // Current = 40

			if(current == NULL)
				return;

			Node<T> *KthNode = current;

			// Take current to last node
			while(current->next != NULL)
				current = current->next;
			// Current = 60

			// Change next of last node to head
			current->next = head;

			// Change head to (k+1)th Node
			head = KthNode->next;

			// Change next of Kth Node to NULL
			KthNode->next = NULL;
		}

		//FLATTEN LINKEDLIST
		/*
			5->10->19	(Sorted Horizontally)
			|	|	|
			\/  \/  \/
			7	20	22	(Sorted Vertically)
			|	|	|
			\/  \/  \/
			30		50

			ans = 5->7->10->19->20->22->30->50 (Sorted)
		*/
		Node<T> *flatten(){
			// Base cases
			if(!head || !(head->right))
				return;

			// Merge this list with list on right side
			return mergeFlatten(head, flatten(head->right));
		}

		private:
		Node<T> *mergeFlatten(Node<T> *a, Node<T> *b){
			// If first list is empty, return second
			if(!a)
				return b;

			// If second list is empty, return first
			if(!b)
				return a;

			// Compare the data memebers of head nodes of
			// both lists and put the smaller one in result
			Node<T> *result;
			if(a->data < b->data){
				result = a;
				result->down = mergeFlatten(a->down, b);
			}
			else{
				result = b;
				result->down = mergeFlatten(a, b->down);
			}
			return result;
		}

		public:
		//GET INSTERSECTION POINT OF TWO LINKED LISTS
		/*
			1
			 \
			  2	  0	 	
			   \ /
			    3
				|
				4
				|
				5

			Ans = 3 (Intersection Point)
		*/
		T getIntersectionPointOfTwoLL(const SinglyLinkedList &L){
			int c1 = this->getLength();
			int c2 = L.getLength();

			if(c1 > c2){
				int diff = c1-c2;
				return getIntersectionPointHelper(diff, head, L.head);
			}
			else{
				int diff = c2-c1
				return getIntersectionPointHelper(diff, L.head, head);
			}
		}

		private:
		T getIntersectionPointHelper(int diff, Node<T> *a, Node<T> *b){
			Node<T> *it1 = a;
			Node<T> *it2 = b;

			while(diff--)
				it1 = it1->next;

			while(it1 && it2){
				if(it1 == it2)
					return it1->data;

				it1 = it1->next;
				it2 = it2->next;
			}
		}

		public:
		//ADD NUMBERS REPRESENTED BY LL(SET 1)
		/*
			first list : 7->5->9->4->6 | represents 64957
			second list: 8->4 | represents 48
			Output
			Resultant List : 5->0->->5->6 | 65005
		*/
		SinglyLinkedList addTwoLists(const SinglyLinkedList &first, const SinglyLinkedList &second){
			SinglyLinkedList resutl;
			Node<T> *prev = NULL;
			int carry=0, sum;

			while(first || second){
				/*
					Calculate value of next digit in resultant list.
					The next digit is the sum of following things
					(i) 	Carry
					(ii)	Next digit of first list (if exists)
					(iii)	Next digit of second list (if exists)
				*/

				sum = carry + ((first) ? first->data : 0 +
								(second) ? second->data : 0);

				//Update carry for next calculation
				carry = (sum>10) ? 1 : 0;

				//Update sum if it is greater than 10
				sum = sum%10;

				//Create new node with sum as data
				Node<T> *temp = new Node<T>(sum);

				//If this is the first node of result
				if(result.head == NULL)
					L.head = temp;
				else
					prev->next = temp;
				
				//Set prev for next insertion
				prev = temp;

				if(first) first = first->next;
				if(second) second = second->next;
			}
			
			if(carry)
				prev->next = new Node<T>(carry);

			return result;
		}

		//ADD TWO NUMBERS REPRESENTED BY LL (SET 2)
		/* First List: 5->6->3 | Represents Number 563
		 * Second List:8->4->2 | Represents Number 842
		 * Output
		 * Resultant List:1->4->0->5 | 1405
		 */

		/* 1.Calculate sizes of given two linked list
		 * 2.If sizes are same, then calculate sum using recursion.
		 *   Hold all nodes in recursion call stack till the
		 *   rightmost node, calculate sum of rightmost nodes and
		 *   forward carry to left side.
		 * 3.If size is not same, then:
		 *      a)Calculate diff of sizes. (diff)
		 *      b)Move diff nodes ahead in the bigger LL.
		 *        Now use step 2 to calculate sum of smaller list
		 *        and right sub-list (of same size) of larger list.
		 *        Also, store the carry of this sum.
		 *      c)Calculate sum of the carry (calculated in prev step)
		 *        with the remaining left sub-list of larger list.
		 *        Nodes of this sum are added at the beginning of
		 *        sum list obtained previous step.
		 */

		 SinglyLinkedList addTwoLists2(const SinglyLinkedList &first, const SinglyLinkedList &second){
			 SinglyLinkedList result;
			 if(!first.head)
				 return second;

			 if(!second.head)
				 return first;

			 int size1 = first.getLength();
			 int size2 = second.getLength();

			 int carry = 0;

			 if(size1 == size2)
				 result.head = addSameSize(first, second, &carry);

			}
			
			else{
				int diff = abs(size1-size2);

				//first list should always be larger than second list.
				//If not, swap.
				if(size1 < size2)
					swapPointer(&L1.head, &L2.head);

				//Move forward diff number of nodes in first list
				for(Node<T> *curr = L1.head; diff--; curr=curr->next);

				//Get addition of same size lists
				result.head = addSameSize(curr, L2.head, &carry);

				//Get addition of remaining first list and carry
				addCarryToRemaining(L1.head, curr, &carry, result);
			}

		private:
		void swapPointer(Node<T> **a, Node<T> **b){
			Node<T> *temp = *a;
			*a = *b;
			*b = temp;
		}

		Node<T> *addSameSize(Node<T> *first, Node<T> *second, int *carry){
			if(!first)
				return second;

			if(!second)
				return first;

			int sum;

			Node<T> *ans = new Node<T>();

			ans->next = addSameSize(first->next, second->next, carry);

			sum = first->data + second->data + *carry;

			*carry = sum/10;
			sum %= 10;

			ans->data = sum;
			return ans;
		}

		void addCarryToRemaining(Node<T> *first, Node<T> *curr, int *carry, SinglyLinkedList &result){
			int sum;
			if(first != curr){
				addCarryToRemaining(first->next, curr, carry, result);

				sum = first->data + *carry;
				carry = sum/10;
				sum %= 10;

				result.insertAtTheHead(sum);
			}
		}

		public:
		//CHECK IF LIST IS PALINDROME
		bool isPalindrome(){
			return isPalindromeHelper(&head, head);
		}

		private:
		bool isPalindromeHelper(Node<T> **left, Node<T> *right){
			//When at lastnode->next i.e. NULL ... return true to backtrack
			if(!right)
				return true;

			//Recurse till the last node
			bool isp = isPalindromeHelper(left, right->next);
			if(!isp)
				return false;

			//Check if data matches at first and last node of sub-list
			bool isp_data = ((*left)->data == right->data);

			*left = (*left)->next;
			return isp_data;
		}

		public:
		//SORT LINKED LIST OF 0s 1s 2s
		/*
			Count the number of 0s=a, 1s=b, 2s=c
			Fill first a nodes with 0
			Next b nodes with 1
			Next c nodes with 2
		*/

		/*
			!-----BY CHANGING LINKS-----!
			Make 3 lists of 0s 1s and 2s
			Link all three lists
		*/
		void sortList0s1s2s(){
			if(!head || !(head->next))
				return;

			// Pointers to 0,1,2 Dummy List
			// Created to avoid any NULL checks
			Node<T> *zeroD = new Node<T>();
			Node<T> *oneD = new Node<T>();
			Node<T> *twoD = new Node<T>();

			Node<T> *zero = zeroD, *one = oneD, *two=twoD;
			Node<T> *curr = head;
			while(curr){
				if(curr->data == 0){
					zero->next = curr;
					zero=zero->next;
				}
				else if(curr->data == 1){
					one->next = curr;
					one = one->next;
				}
				else{
					two->next = curr;
					two=two->next;
				}
				curr = curr->next;
			}

			//Attach three lists
			zero->next = (oneD->next) ? (oneD->next) : (twoD->next);
			one->next = twoD->next;
			two->next = NULL;

			//Update head
			head = zeroD->next;

			//Delete dummy nodes
			delete zeroD;
			delete oneD;
			delete twoD;
		}

		//GIVEN POINTER TO THE GIVEN NODE, DELETE IT, W/O HEAD POINTER GIVEN
		void deleteNode(Node<T> *delNode){
			if(delNode->next == NULL)
				cout << "Can't delete last node with these constraints" << endl;

			else{
				delNode->data = delNode->next->data;
				Node<T> *temp = delNode->next;
				delNode->next = temp->next;
				delete temp;
			}
		}

		//PARTITION LIST
		/*Given a linked list and a value x, 
		 * partition it such that all nodes less than x 
		 * come before nodes greater than or equal to x.
		 * You should preserve the original relative order 
		 * of the nodes in each of the two partitions.
		 * For example,
		 * Given 1->4->3->2->5->2 and x = 3,
		 * return 1->2->2->4->3->5.
		 *
		 * Sol: Make 2 different list
		 *      i) Containing Values  <  x
		 *      ii)Containing Values  >= x
		 *      Join the two list and return.
		 */
		Node<T> *partition(Node<T> *head, int x){
			Node<T> node1(0), node2(0);

			Node<T> *p1 = &node1, *p2 = &node2;
			while(head){
				if(head->data < x)
					p1 = p1->next = head; // compiler traverses it left to right
					// Equivalent to:
					//p1->next = head
					//p1 = p1->next
				else
					p2 = p2->next = head;
				head = head->next;
			}
			p2->next = NULL;
			p1->next = node2.next;
			return node1.next;
		 }

		//GET SHARED MEMORY DATA
		T getDataForSharedMemory() const {
			return sharedMemory;
		}

		//INCREMENT SHARED MEMORY DATA
		void incrementSharedMemory(){
			sharedMemory++;
		}

		//FRIEDNT FUNCTION DECLARATION
		friend void friendFunction();

		//FRIEND FUNCTION DECLARATION | OPERATOR OVERLOADING
		template<typename U>
		friend ostream &operator<<(ostream &, const SinglyLinkedList<U> &);
};

//STATIC MEMBER INITITALIZATION
template<typename T>
int SinglyLinkedList<T>::sharedMemory=10;

//OPERATOR OVERLOADING FUNCTION (FRIEND TO CLASS)
template<typename T>
ostream & operator<<(ostream &out, const SinglyLinkedList<T> &L){
	const Node<T> *it = L.head;
	while(it){
		out << it->data << " --> ";
		it = it->next;
	}
	cout << "NULL" << endl;
	return out;
}

//FUNCTION (FRIEND TO CLASS)
void friendFunctiona(){
	//Do any work.
	return;
}

//FUNCTION DEFITION OF INSERTATTHEEND
template<typename T>
void SinglyLinkedList<T>::insertAtTheEnd(const T &d){
	Node<T> *newNode = new Node<T>(d);
	Node<T> *temp = head;
	if(!temp){
		head = newNode;
	} else {
		while(temp->next){
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return;
}

int &getValue(){
	static int x = 10;
	x++;
	return x;
}

#endif

#include <iostream>
#include <vector>
using namespace std;

bool isArrayInSortedOrder(vector<int> A, int n){
	if(n==1)
		return true;
	
	return( (A[n-1]<A[n-2]) ? false : isArrayInSortedOrder(A, n-1));
}

int main(){
	cout << "Enter Size ";
	int size;
	cin >> size;
	vector<int> A;
	
	cout << "Enter Array Data" << endl;
	int data;
	for(int i=0; i<size; i++){
		cin>>data;
		A.push_back(data);
	}

	if(isArrayInSortedOrder(A, size)) 
		cout << "Sorted" << endl; 
	else 
		cout << "Not Sorted" << endl;
	
	return 0;
}

/*Calculate Factorial of a Number*/
#include <iostream>
#include <vector>

using namespace std;

// Recursive Solution
long factRecursive(long input){
	if(input == 0 || input == 1){
		return 1;
	}
	else{
		return input*factRecursive(input-1);
	}
}

// Iterative Solution
long factIterative(long input){
	long res = 1;
	for(long i=2; i<=input; i++){
		res *= i;
	}
	return res;
}

// Using Memoization
long factMemoization(long input){
	vector<long> res(2, 1);
	if(input > 0){
		for(int i=2; i<=input; i++){
			res.push_back(i * res.at(i-1));
		}
	}
	return res.at(input);
}

int main(){
	long input = 0;
	cout<<"Enter input: ";
	cin>>input;
	cout << factMemoization(input)<<endl;
	return 0;
}

#include <iostream>

using namespace std;

void towersOfHanoi(int n, char fromPeg, char toPeg, char auxPeg){
	if(n == 1){
		cout<<"Move disk 1 from " << fromPeg << " to " << toPeg <<endl;
		return;
	}
	
	towersOfHanoi(n-1, fromPeg, auxPeg, toPeg);

	cout<<"Move disk " << n << " from " << fromPeg << " to " << toPeg << endl;

	towersOfHanoi(n-1, auxPeg, toPeg, fromPeg);
}

int main(){
	cout << "Enter number of disks: ";
	long input;
	cin>>input;
	towersOfHanoi(input, 'A', 'B', 'C');
	return 0;
}

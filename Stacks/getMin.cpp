/* Implement SpecialStack that returns
 * minimum element from stack in O(1)
 */

/* 16 --> Top | getMin() returns 15.
 * 15     Now we do 2 pop()
 * 29
 * 19
 * 18
 * -----------------------------
 * 29 --> Top | getMin() returns 18.
 * 19
 * 18
 */

// Approach 1 | O(n) space complexity

class SpecialStack : public Stack {
	//Auxilliary Stack to Store Min.
	Stack min;

	public:
	void pop(){
		if(!isEmpty()){
			Stack::pop();
			min.pop();
		}
	}

	void push(int x){
		if(isEmpty()){
			Stack::push(x);
			min.push(x);
			return;
		}
		else{
			Stack::push(x);
			int y = min.top();
			if(x < y)
				min.push(x);
			else
				min.push(y);
		}
	}

	int getMin(){
		return min.top();
	}
    /*-----Slightly Space Optimized-----*/
    /* Push only when the incoming element
     * of main stack is smaller or equal to
     * top of auxiliary stack
     */
    /* Pop element from auxiliary stack only
     * if it is equal to that from main stack
     */

};
// Approach 2 | O(1) Space Complexity
// http://www.geeksforgeeks.org/design-a-stack-that-supports-getmin-in-o1-time-and-o1-extra-space/
/* We define a variable minEl that stores the
 * current minimum element that stores the 
   current minimum element in the stack. 
   Now the interesting part is, how to handle 
   the case when minimum element is removed. 
   To handle this, we push "2x-minEl" into the
 * stack instead of x so that previous min
 * element can be retrieved using current
 * minEl and its value stored in stack.
 */
/* Push(x)
 * 1.If stack is empty, insert x into stack and
 *    make minEl = x
 * 2.If stack is !empty, compare x with minEl.
 *  a.)x >= minEl : simply insert x.
 *  b.)x < minEl : insert(2*x - minEl) into the
 *     stack and make minEl = x.
 *
 * Pop()
 * Remove element from top. Let the removed el
 * be 'y'.
 *  a.)y>=minEl : minimum element in the stack
 *                is still minEl.
 *  b.)y<minEl : the minimum element now becomes
 *     (2*minEl - y), so update (minEl = 2*minEl-y)
 *     This is where we retrieve previous minimum
 *     from current minimum and its value in stack.
 *     For example, let the element to be removed
 *     be 1 and minEl be 2. We remove 1 and update
 *     minEl as 2*2-1 = 3;
 */
/* IMPORTANT POINTS 
 * 1. Stack doesn't hold actual value of an element
 *    if it is minimum so far.
 * 2. Actual minimum element is always stored in minEl
 */

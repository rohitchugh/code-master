#include <iostream>
#include <stack>

using namespace std;

bool isOperand(char ch) {
	return ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'));
}

int precedence(char a) {
	if(a == '^')
		return 3;
	if(a == '/' || a=='*')
		return 2;
	if(a == '+' || a=='-')
		return 1;
	return -1;
}

void infixToPostfix(string a, string &b) {
	stack<char> s1;
	string::iterator it;
	cout << "Infix: " << a << endl;
	
	for(it = a.begin(); it != a.end(); it++) {
		if(isOperand(*it)){
			b += (*it);
		}
		else if(*it == '(') {
			s1.push(*it);
		}
		else if(*it == ')') {
			while(!s1.empty() && s1.top() != '('){
				b += s1.top();
				s1.pop();
			}
			if(!s1.empty() && s1.top() != '(') {
				cout << "Invalid Expression" << endl;
				return;
			}
			s1.pop();
		}
		else {
			while(!s1.empty() && precedence(s1.top()) >= precedence(*it)) {
				b += s1.top();
				s1.pop();
			}
			s1.push(*it);
		}
	}

	while(!s1.empty()){
		b += s1.top();
		s1.pop();
	}
}

int main() {
	string a = "(A+B)*(C+D)";
	string b = "";
	infixToPostfix(a,b);
	cout << "Postfix : " << b << endl;
	return 0;
}

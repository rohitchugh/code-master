/* The Next greater Element for an element x
 * is the first greater element on the right
 * side of x in array. Elements for which no
 * greater element exist, consider next
 * greater element as -1
 */

/* Method 1 is simple approach of two nested
 * loops to find the next greater element
 * for each element. If no such element is
 * found then return -1
 */

/* Method 2  | O(n)
 * Worst Case when elements in decreasing
 * sorted order. Elements accessed at most
 * 4 times
 * a) Initially pushed to the stack.
 * b) Popped from the stack when next element
 *    is being processed.
 * c) Pushed back to the stack because next
 *    element is smaller.
 * d) Popped from the stack in step 3 of algo.
 */
#include <iostream>
#include <stack>
#define SIZE 100

using namespace std;

void printNGE(int arr[], int n){
	int top = -1;
	int element, next;
	stack<int> s;

	s.push(arr[0]);

	for(int i=1; i<n; i++){
		next = arr[i];

		if(!s.empty()){
			element = s.top();
			s.pop();
			/*
				Get all elements from stack smaller
				than next, print and remove them
			*/

			while(element < next) {
				cout << element << " ---- " << next << endl;
			
				if(s.empty())
					break;

				element = s.top();
				s.pop();
			}
			
			/*
				If the top element is greater than next
				then push it back
			*/
			if(element > next)
				s.push(element);
		}
		/*Push next in stack for processing*/
		s.push(next);
	}

	/*For the remaining elements in stack NGE is -1*/
	while(!s.empty()){
		cout << s.top() << " ---- " << -1 << endl;
		s.pop();
	}
}

int main(){
	int arr[] = {
		11, 13, 21, 3
	};

	int n = sizeof(arr)/sizeof(arr[0]);
	printNGE(arr, n);
	return 0;
}

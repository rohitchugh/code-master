#include <iostream>
#include <stack>

using namespace std;

int evaluate(string &a) {
	stack<int> s1;
	string::iterator it;

	for(it = a.begin(); it!=a.end(); it++) {
		if(isdigit(*it)) {
			s1.push((*it)-'0');
		}
		else {
			int a = s1.top();
			s1.pop();
			int b = s1.top();
			s1.pop();
			switch(*it) {
				case '+' :
					s1.push(b+a);
					break;
				case '-':
					s1.push(b-a);
					break;
				case '*':
					s1.push(b*a);
					break;
				case '/':
					s1.push(b/a);
					break;
			}
		}
	}
	return s1.top();
}

int main(){
	string a = "231*+9-";
	cout << evaluate(a) << endl;
	return 0;
}

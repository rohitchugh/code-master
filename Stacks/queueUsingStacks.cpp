/* Method 1 (By making enQueue operation costly) 
    This method makes sure that oldest entered element is always
    at the top of stack 1, so that deQueue operation just pops
    from stack 1. To put element at top of stack 1, stack 2 is
    used.

    enQueue(q,x)
        1. While !stack1.empty(), stack2.push(stack1.pop())
        2. stack1.push(x)
        3. While !stack2.empty(), stack1.push(stack2.pop())


    deQueue(q)
        1. If stack1.empty() : Error
        2. stack1.pop()
    
    front()
        return stack1.top()
*/
/* Better 
Method 2 (By making deQueue operation costly)    
    Enqueue operation pushes new element at top of stack1.
    While dequeue, move all elements from 1 to 2 and then
    pop top of stack 2.

    enQueue(q, x)
        stack1.push(x)

    deQueue(q)
        1. stack1.empty() && stack2.empty() : Error
        2. if stack2.empty() : while !stack1.empty()
                                stack2.push(stack1.pop())
        3. stack2.pop()                               
*/

#include <iostream>
#include <stack>
using namespace std;

class queueUsingStack{
	stack<int> main;
	stack<int>	aux;

	public:
	queueUsingStack(){};

	void enQueue(int d){
		while(!main.empty()){
			aux.push(main.top());
			main.pop();
		}
		main.push(d);
		while(!aux.empty()){
			main.push(aux.top());
			aux.pop();
		}
	}

	void deQueue(){
		if(!main.empty())
			main.pop();
	}

	int front(){
		if(!main.empty())
			return main.top();
		return -1;
	}
};

int main(){
	queueUsingStack stack;
	stack.enQueue(1);
	stack.enQueue(2);
	stack.enQueue(3);
	stack.enQueue(4);
	
	cout << stack.front() << endl;

	stack.deQueue();
	stack.deQueue();
	stack.deQueue();
	
	cout << stack.front() << endl;
	stack.deQueue();
	stack.deQueue();
	stack.deQueue();
	stack.deQueue();
	stack.deQueue();
	
	cout << stack.front() << endl;
	return 0;
}

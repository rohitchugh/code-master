#include <iostream>
#include <stack>

using namespace std;

void reverse(string &a){
	stack<char> s;
	string::iterator it;

	for(it = a.begin(); it!=a.end(); it++){
		s.push(*it);
	}
	a = "";
	while(!s.empty()){
		a += s.top();
		s.pop();
	}
}

int main(){
	string a = "ABCDEFGH";
	reverse(a);
	cout << a << endl;
	return 0;
}
